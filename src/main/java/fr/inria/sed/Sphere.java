package fr.inria.sed;

import javax.vecmath.Point3d;

/**
 * Created by dparsons on 12/08/15.
 */
public class Sphere {
  // ***********************************************************************
  //                               Constructors
  // ***********************************************************************
  public Sphere() {
    radius_ = 0.0;
    pos_ = new Point3d();
  }

  public Sphere(double radius) {
    radius_ = radius;
    pos_ = new Point3d();
  }

  public Sphere(double radius, Point3d pos) {
    radius_ = radius;
    pos_ = pos;
  }


  // ***********************************************************************
  //                                 Methods
  // ***********************************************************************
  public double computeVolume() {
    return 4 * Math.PI * Math.pow(radius_, 3) / 3;
  }

  public double distanceTo(Sphere other) {
    return pos_.distance(other.pos_);
  }

  public double distanceSquaredTo(Sphere other) {
    return pos_.distanceSquared(other.pos_);
  }

  public boolean inContactWith(Sphere other) {
    return distanceSquaredTo(other) <= Math.pow(radius_ + other.radius_, 2);
  }

  // ***********************************************************************
  //                                Accessors
  // ***********************************************************************
  public Point3d getPos() {
    return pos_;
  }

  public double getRadius() {
    return radius_;
  }

  // ***********************************************************************
  //                               Attributes
  // ***********************************************************************
  protected double radius_ = 0.0;
  protected Point3d pos_;
}
