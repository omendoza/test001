package fr.inria.sed;

import org.junit.Test;
import javax.vecmath.Point3d;
import static org.junit.Assert.*;

/**
 * Created by dparsons on 12/08/15.
 */
public class SphereTest {
  @Test
  public void testComputeVolume() throws Exception {
    Sphere mySphere = new Sphere(1.5);
    System.out.println("***********************************************************");
    System.out.println("SPHERE TEST");
    System.out.println("***********************************************************");
    assertEquals(14.137166941154069, mySphere.computeVolume(), 0.0);
  }
}